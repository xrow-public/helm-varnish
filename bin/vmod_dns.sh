#!/bin/bash

set -eux

VMOD_DIRECTORY="/usr/lib64/varnish/vmods"

yum -y install patch git varnish-devel python3 python3-pip python3-docutils make automake gcc libtool 

ln -s /usr/bin/libtoolize /usr/bin/glibtoolize || true
rm -Rf libvmod-dns
git clone https://github.com/knq/libvmod-dns.git
pushd libvmod-dns
git checkout fb2d9a631ae0824d6d10fe1a23ce5db9bcc508d5 # There are no tags or releases available, so better use a specific commit hash to avoid unexpected breakage
sed -i 's/google-public-dns-a.google.com/dns.google/g' src/vtc/vmod_dns02.vtc
sed -i 's/google-public-dns-b.google.com/dns.google/g' src/vtc/vmod_dns02.vtc

sh bootstrap
sh configure
make
make check
make install

popd

ldd $VMOD_DIRECTORY/libvmod_dns.so
