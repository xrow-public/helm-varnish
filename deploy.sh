#!/bin/bash

RELEASE="test"
SERVER="07.xrow.net"
CHART_DIR="./chart/"
NAMESPACE="test-varnish"
ENV="dev"

oc config use-context $SERVER
oc new-project $NAMESPACE || true
helm template $RELEASE $CHART_DIR -n $NAMESPACE -f deploy/$ENV.yaml

#oc apply -f deploy/test.cr.yaml
helm upgrade --install $RELEASE $CHART_DIR --debug -n $NAMESPACE -f deploy/$ENV.yaml