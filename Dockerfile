FROM quay.io/centos/centos:stream8

ENV BACKEND_HOST=localhost
ENV BACKEND_PORT=8080
ENV BACKEND_PROBE_HOSTNAME=""
ENV BACKEND_PROBE_PATH=""
ENV MEMORY="1000m"
ENV USER_HASH="ecaea5a638cb64ce41e9266e550963228d0bb58ed86ca7278f1b3e135c155669"
ENV SECRET="qHWOWCpP8g2dEzumQHV0KSkkr7AGQkYY"
ENV container=docker

RUN yum -y update && \
    dnf -y module disable varnish

RUN dnf module -y install python38

RUN yum install -y gettext nc

RUN pip3 install docutils Sphinx

RUN curl -s https://packagecloud.io/install/repositories/varnishcache/varnish66/script.rpm.sh | bash
# the epel repo contains jemalloc
RUN yum install -y epel-release
# install our dependencies
RUN yum install -y git make automake libtool varnish-devel
# download the top of the varnish-modules 6.6 branch
RUN git clone --branch 6.6 --single-branch https://github.com/varnish/varnish-modules.git
# jump into the directory
WORKDIR /varnish-modules
# prepare the build, build, check and install
RUN ./bootstrap && \
    ./configure && \
    make && \
    make check -j 4 && \
    make install

RUN yum clean all

ADD bin/uid_entrypoint /bin/uid_entrypoint

# Broken DNS mod 
# RUN /bin/bash ${APP_ROOT}/bin/vmod_dns.sh

RUN chgrp -R 0 /etc/varnish && \
    chmod -R g+rwX /etc/varnish

WORKDIR /var/lib/varnish

EXPOSE 6081 6082 8443

RUN chmod -R 755 /bin/uid_entrypoint && \
    chmod -R 777 /etc/varnish && \
    chmod -R g=u /etc/varnish /var/lib/varnish /etc/passwd

ENTRYPOINT [ "/bin/uid_entrypoint" ]

CMD []

USER 1001
