vcl 4.1;

import std;
{{ if .Values.settings.bot_authenticity_rules }}
import dns;
{{ end }}
import cookie;
import xkey;
import directors;
{{ if .Values.settings.bot_detect }}
include "devicedetect.vcl";
{{ end }}
include "ez.vcl";

# See Flow
# https://www.varnish-cache.org/trac/wiki/VarnishInternals
# Tests
# curl 'http://localhost:80/' -H"Host: stable.example.com" --compressed -I
# curl 'http://localhost:8080/' -H"Host: stable.example.com" --compressed -I

# curl http://localhost:8080/_fos_user_context_hash -H"Host: www.wuv.de" -H"X-Forwarded-Proto: https" -H"accept: application/vnd.fos.user-context-hash" -H"x-fos-original-url: /test/login" -H"cookie: eZSESSID=mem54vuus25hnpdj299j91itk2" -I

# curl http://172.30.15.100:80/_fos_user_context_hash -H"X-Forwarded-Proto: https" -H"Host: wuv-test02.wuv.de" -H"accept: application/vnd.fos.user-context-hash" -H"x-fos-original-url: /test/login"

#ab -n 20000 -c 1 -H 'Host: stable.example.com' -H 'cookie: eZSESSID=mem54vuus25hnpdj299j91itk2' 'http://localhost:80/'

# watch "curl 'http://localhost:80/' -H'Host: stable.example.com' -H'cookie: eZSESSID=mem54vuus25hnpdj299j91itk2' --compressed -I"

#{% if varnish_server_name is defined %}{% set Server = varnish_server_name %}{% else %}{% set Server = "Apache" %}{% endif %}
#{% if varnish_x_powered_by is defined %}{% set X_Powered_By = varnish_x_powered_by %}{% else %}{% set X_Powered_By = "xrow GmbH" %}{% endif %}

backend default {
    .host = "${BACKEND_HOST}";
    .port = "${BACKEND_PORT}";
    .connect_timeout = 3s;
    .first_byte_timeout = 120s;
    .between_bytes_timeout = 120s;
{{ if .Values.probe.enabled }}
    .probe = {
        .timeout = 5s;
        .interval = 5s;
        .threshold = 3;
        .request =
            "GET " + ${BACKEND_PROBE_PATH} + " HTTP/1.1"
            "Host: " + ${BACKEND_PROBE_HOSTNAME}
            "Accept-Charset: utf-8"
            "X-FORWARDED-PROTO: https"
            "Connection: close" ;
    }
{{ end }}
}

// ACL for invalidators IP
acl invalidators {
{{- range $index, $invalidator := .Values.settings.invalidators }}
    "{{ $invalidator }}";
{{- end }}
}

acl lbs {
{{- range $index, $lb := .Values.settings.lbs }}
    "{{ $lb }}";
{{- end }}
}

sub vcl_init {
    new pool = directors.round_robin();
    pool.add_backend(default);
}
sub vcl_recv {
    include "filter.vcl";

{{/* TODO

{% if varnish_bot_authenticity_rules %}
    # Verify bot authenticity
    if ({% if varnish_bot_authenticity_urls %}req.url ~ "^({% for url in varnish_bot_authenticity_urls %}{{ url }}{% if not loop.last %}|{% endif %}{% endfor %})" && {% endif %}req.http.user-agent ~ "({% for acl in varnish_bot_authenticity_rules %}{{ acl.match_header }}{% if not loop.last %}|{% endif %}{% endfor %})") {
        # Do a reverse lookup on the client.ip (X-Forwarded-For)
        set req.http.X-Crawler-DNS-Reverse = dns.rresolve(req.http.X-Forwarded-For);

{% for acl in varnish_bot_authenticity_rules %}
        # Check if if requests user-agent header matches "{{ acl.match_header }}"
        if (req.http.user-agent ~ "{{ acl.match_header }}") {
{% if acl.reverse_dns_regex is defined %}
            # Check that the reverse DNS points to an allowed domain
            if (req.http.X-Crawler-DNS-Reverse !~ "{{ acl.reverse_dns_regex }}") {
                return (synth(403, "Forbidden"));
            }

            # Do a forward lookup on the DNS
            set req.http.X-Crawler-DNS-Forward = dns.resolve(req.http.X-Crawler-DNS-Reverse);

            # If the client.ip/X-Forwarded-For doesn't match, then the user-agent is a fake
            if (req.http.X-Crawler-DNS-Forward != req.http.X-Forwarded-For) {
                return (synth(403, "Forbidden"));
            }
{% endif %}
        }

{% endfor %}
        set req.http.cookie = "eZSESSID=valid_bot";
    }
{% endif %}
*/}}
    # Varnish, in its default configuration, sends the X-Forwarded-For header but does not filter out Forwarded header
    # To be removed in Symfony 3.3
    # https://github.com/symfony/symfony/blob/3609744ab8763556b2eff1431424668f825dfb8d/src/Symfony/Component/HttpFoundation/Request.php#L1985
    # https://github.com/ezsystems/ezplatform-ee/blob/caf28ea59ac198aba85c0cceb09852f6e1f32dc0/doc/varnish/vcl/varnish4_xkey.vcl#L31
    unset req.http.Forwarded;


    set req.http.X-grace = "none";
    if (req.proto ~ "HTTP/2") {
        set req.http.X-Forwarded-Proto = "https";
        set req.http.X-Forwarded-Port = "443";
    }
    if (req.method != "GET" &&
        req.method != "HEAD" &&
        req.method != "PUT" &&
        req.method != "POST" &&
        req.method != "OPTIONS" &&
        req.method != "BAN" &&
        req.method != "PURGE" &&
        req.method != "PURGEKEYS" &&
        req.method != "COPY" &&
        req.method != "PATCH" &&
        req.method != "DELETE")
    {
        /* Non-RFC2616 or CONNECT which is weird. */
        return (synth(403, "Method Banned"));
    }

    if (req.restarts == 0) {
        set req.http.X-Forwarded-For = regsuball( req.http.X-Forwarded-For, "[,]+.*$", "");
        if ( !( req.http.X-Forwarded-For && client.ip ~ lbs ) ) {
            set req.http.X-Forwarded-For = client.ip;
        }
    }

    set req.backend_hint = pool.backend();

    if ( (req.url ~ "^/api/ezp/" || req.url ~ "^/ez/notifications" ) && req.url !~ "^/api/ezp/v2/menu" ) {
        return (pipe);
    }
    if ( req.http.x-siteaccess ) {
        return (synth(403, "x-siteaccess is a security problem."));
    }
    if (req.url ~ "^/varnish-status") {
    
        if (std.file_exists("/etc/varnish/deployment")) {
            return (synth(500, "Deployment running. Stay tuned."));
        }
        if(std.healthy(req.backend_hint) ){
            return (synth(200, "OK."));
        }
        else {
            return (synth(500, "No Backends available."));
        }
        return (synth(200, "OK."));
    }
    // Websocket support
    if (req.http.Upgrade ~ "(?i)websocket") {
        return (pipe);
    }
    # normalize encoding fpr less cached versions
    if (req.http.Accept-Encoding) {
        if (req.url ~ "^[^?]*\.(jpg|png|pdf|gif|mp[34]|rar|tar|tgz|gz|wav|zip|bz2|xz|7z|avi|mov|og[gm]|mpe?g|mk[av]|flac|flv|webm)(\?.*)?$") {
            # No point in compressing these
            unset req.http.Accept-Encoding;
        }
        elsif (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        }
        elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        }
        else {
            # unkown algorithm
            unset req.http.Accept-Encoding;
        }
    }
    {{ if .Values.settings.vcl_recv }}
      {{- .Values.settings.vcl_recv | indent 4 -}}
    {{ end }}

    # If client enforces cache control, we ignore it.
    if (req.http.Cache-Control) {
        unset req.http.Cache-Control;
    }

    if ( "${BACKEND_PROBE_PATH}" != "" && req.url ~ "${BACKEND_PROBE_PATH}" ) {
        return (pass);
    }

    # Normalize the header, remove the port (in case you're testing this on various TCP ports)
    set req.http.Host = regsub(req.http.Host, ":[0-9]+", "");

    # Normalize the query arguments
    set req.url = std.querysort(req.url);

    // Trigger cache purge if needed
    call ez_purge;
    // Normalize cookies and just pass session cookie
    if (req.http.cookie) {
        cookie.parse(req.http.cookie);

        # or delete all but a few:
        cookie.keep_re("^eZSESSID");

        # Store it back into req so it will be passed to the backend.
        set req.http.cookie = cookie.get_string();

        # If empty, unset so the builtin VCL can consider it for caching.
        if (req.http.cookie == "") {
            unset req.http.cookie;
        }
    }
    
    # All static
    # Do a standard lookup on assets
    if (req.url ~ "^/(assets|bundles|var|js|css|design|extension/[a-z]+/design)/") {
        unset req.http.Cookie;
        return (hash);
    }

    if (req.method != "GET" && req.method != "HEAD") {
        /* We only deal with GET and HEAD by default */
        return (pass);
    }
    # Make large files pass
    if (req.url ~ "^[^?]*\.(mp[34]|rar|tar|tgz|gz|pdf|wav|zip|bz2|xz|7z|avi|mov|og[gm]|mpe?g|mk[av]|flac|flv|webm)(\?.*)?$") {
        return (pass);
    }
    if (!req.url ~ "/content/preview/") {
        set req.http.Surrogate-Capability = "abc=ESI/1.0";
    }
    {{ if .Values.settings.ezplatform.hash }}
    // Retrieve client user hash and add it to the forwarded request.
      call ez_user_context_hash;
    {{ end }}

    {{ if .Values.settings.bot_detect }}
      call devicedetect;
    {{ end }}

    return (hash);
}
sub vcl_hash {

    hash_data(req.url);
    if (req.http.host) {
        hash_data(req.http.host);
    } else { 
        hash_data(server.ip); 
    }
    # You can disable this if sslzones are off in eZ Publish
    if( req.http.X-Forwarded-Proto ) {
        hash_data(req.http.X-Forwarded-Proto);
    }
    if( req.http.x-user-context-hash ) {
        hash_data(req.http.x-user-context-hash);
    }
    if( req.http.X-Siteaccess ) {
        hash_data(req.http.X-Siteaccess);
    }
    return (lookup);
}

sub vcl_backend_fetch {
    set bereq.http.connection = "close";  
    return (fetch);
}
sub vcl_backend_response {

    if (bereq.http.accept ~ "application/vnd.fos.user-context-hash" && beresp.status >= 500
    ) {
        set beresp.http.user-context-error = "1";
        std.syslog( 3, "User context error in on Host " + bereq.http.Host + " for URL "  + bereq.http.x-fos-original-url );
        return (abandon);
    }
    if (bereq.http.accept ~ "application/vnd.fos.user-context-hash") {
        std.log( "User hash: " + beresp.http.x-user-context-hash );
    }
    set beresp.http.X-Backend = beresp.backend.name;
    if (beresp.http.Surrogate-Control ~ "ESI/1.0") {
        unset beresp.http.Surrogate-Control;
        set beresp.do_esi = true;
    }
    if (bereq.uncacheable) {
        return (deliver);
    } else if (beresp.ttl <= 0s ||
      beresp.http.Set-Cookie ||
      beresp.http.Surrogate-control ~ "(?i)no-store" ||
      (!beresp.http.Surrogate-Control &&
        beresp.http.Cache-Control ~ "(?i:no-cache|no-store|private)") ||
      beresp.http.Vary == "*") {
        # Mark as "Hit-For-Miss" for the next 2 minutes
        set beresp.ttl = 120s;
        set beresp.uncacheable = true;
    } else if ( beresp.status == 200 && beresp.ttl < {{.Values.settings.ttl}}s ) {
        set beresp.ttl = {{.Values.settings.ttl}}s;
    } else if (beresp.status == 404) {
       # @TODO needs to be disabled and the helm chats need to implement after deplyoment "varnishadm ban obj.status == 404" 
       # set beresp.ttl = 600s;
    } else if (beresp.status == 301 && !beresp.http.Set-Cookie) {
        set beresp.ttl = 4h;
        unset beresp.http.Cache-Control;
    } else if (beresp.status == 302) {
        # Fix for ezplatform caching 302 redirects
        set beresp.uncacheable = true;
    }

    
    set beresp.grace = {{.Values.settings.max_grace}}h;
    if (beresp.http.etag) {
        unset beresp.http.etag;
    }
    # Large static files are delivered directly to the end-user without
    # waiting for Varnish to fully read the file first.
    # Varnish 4 fully supports Streaming, so use streaming here to avoid locking.
    if (bereq.url ~ "^[^?]*\.(jpg|png|gif|mp[34]|rar|tar|tgz|gz|wav|zip|bz2|xz|7z|avi|mov|og[gm]|mpe?g|mk[av]|flac|flv|webm)(\?.*)?$") {
        unset beresp.http.set-cookie;
        set beresp.do_stream = true; # Check memory usage it'll grow in fetch_chunksize blocks (128k by default) if the backend doesn't send a Content-Length header, so only enable it for big objects
        # Don't try to compress it for storage
        set beresp.do_gzip = false;  
        set beresp.do_gunzip = false;
    }
    # Only set cache if it is not defeined.
    if (bereq.url ~ "^/var/" && beresp.ttl < 1s ) {
        unset beresp.http.expires;
        set beresp.http.cache-control = "public, max-age=2592000";
    }

    # We can remove expire when we have Cache-Control
    if (beresp.http.Expires && beresp.http.Cache-Control) {
        unset beresp.http.Expires;
    }
    
    // Compressing the content
    if (beresp.http.Content-Type ~ "application/javascript"
        || beresp.http.Content-Type ~ "application/json"
        || beresp.http.Content-Type ~ "application/vnd.ms-fontobject"
        || beresp.http.Content-Type ~ "application/vnd.ez.api"
        || beresp.http.Content-Type ~ "application/x-font-ttf"
        || beresp.http.Content-Type ~ "image/svg+xml"
        || beresp.http.Content-Type ~ "text"
    ) {
        set beresp.do_gzip = true;
    }
    {{ if .Values.settings.vcl_backend_response }}
      {{- .Values.settings.vcl_backend_response | indent 4 -}}
    {{ end }}

    set bereq.http.connection = "close";

    return (deliver);
}
sub vcl_hit {
    set req.http.X-TTL = obj.ttl;
    if (obj.ttl >= 0s) {
        # normal hit
        return (deliver);
    }

    # We have no fresh fish. Lets look at the stale ones.
    if (std.healthy(req.backend_hint)) {
        # Backend is healthy. Limit age to {{.Values.settings.max_stale}}s.
        if (obj.ttl + {{.Values.settings.max_stale}}s > 0s) {
            set req.http.X-grace = "normal(limited)";
            return (deliver);
        }
        else {
            # No candidate for grace. Fetch a fresh object.
            return (deliver);
        }
    } else {
        # backend is sick - use full grace
        if (obj.ttl + obj.grace > 0s) {
            set req.http.X-grace = "full";
            return (deliver);
        }
        else {
            # no graced object.
            return (deliver);
        }
    }
}
sub vcl_deliver {

    // On receiving the invalidate token response, copy the invalidate token to the original
    // request and restart.
    if (req.restarts == 0 && resp.http.content-type ~ "application/vnd.ezplatform.invalidate-token" ) {
        set req.http.x-backend-invalidate-token = resp.http.x-invalidate-token;
        return (restart);
    }

    if (resp.http.user-context-error) {
        return (deliver);
    }
    // On receiving the hash response, copy the hash header to the original
    // request and restart.
    if (req.restarts == 0 && resp.http.content-type ~ "application/vnd.fos.user-context-hash" ) {
        set req.http.x-user-context-hash = resp.http.x-user-context-hash;
        return (restart);
    }

    // If we get here, this is a real response that gets sent to the client.

    // Remove the vary on user context hash, this is nothing public. Keep all
    // other vary headers.
    if (resp.http.Vary ~ "X-User-Context-Hash") {
        set resp.http.Vary = regsub(resp.http.Vary, "(?i),? *X-User-Context-Hash *", "");
        set resp.http.Vary = regsub(resp.http.Vary, "^, *", "");
        if (resp.http.Vary == "") {
            unset resp.http.Vary;
        }

        // If we vary by user hash, we'll also adjust the cache control headers going out by default to avoid sending
        // large ttl meant for Varnish to shared proxies and such. We assume only session cookie is left after vcl_recv.
        if (req.http.cookie) {
            // When in session where we vary by user hash we by default avoid caching this in shared proxies & browsers
            // For browser cache with it revalidating against varnish, use for instance "private, no-cache" instead
            set resp.http.cache-control = "private, no-cache, no-store, must-revalidate";
        } else if (resp.http.cache-control ~ "public") {
            // For non logged in users we allow caching on shared proxies (mobile network accelerators, planes, ...)
            // But only for a short while, as there is no way to purge them
            set resp.http.cache-control = "public, s-maxage=600, stale-while-revalidate=300, stale-if-error=300";
        }
    }
    set resp.http.Vary = regsub(resp.http.Vary, "(,| *)(?i)X-Forwarded-Proto(,| *)", "");

    if (resp.http.Vary == "") {
        unset resp.http.Vary;
    }
    if (resp.http.Server) {
        unset resp.http.Server;
    }
    if (resp.http.served-by) {
        unset resp.http.served-by;
    }
    if (resp.http.Via) {
        unset resp.http.Via;
    }
    if (resp.http.X-Location-Id) {
        unset resp.http.X-Location-Id;
    }
    #If Debug turn on
    if (resp.http.X-Varnish) {
        unset resp.http.X-Varnish;
    }
    if (resp.http.Content-Type && resp.http.Content-Type ~ "text/html") {
        unset resp.http.Last-Modified;
        unset resp.http.Expires;
        set resp.http.Cache-Control = "max-age=0, no-cache, no-store, must-revalidate";
        set resp.http.Pragma = "no-cache";
    }

    # Fix broken Last-Modified
    if ( resp.http.Last-Modified == "" ) {
        unset resp.http.Last-Modified;
    }

    if ( resp.status == 301 && resp.http.Cache-Control ~ "no-cache" )
    {
        set resp.http.Cache-Control = "public, max-age=86400";   
    }

    # We do not want to expire all at once; Age > max-age
    set resp.http.Age = "0";
    {{ if .Values.settings.powered_by }}
    set resp.http.X-Powered-By = "{{ .Values.settings.powered_by }}";
    {{ end }}
    unset resp.http.Via;
    # Ensure set-cookie is never present on cached requests
    if (resp.http.Set-Cookie && obj.hits > 0) {
      unset resp.http.Set-Cookie;
    }
    # Debug Header
    set resp.http.X-Cache = server.hostname;

    if (resp.http.X-Backend) {
        set resp.http.X-Cache =  resp.http.X-Cache + ":" + resp.http.X-Backend;
        unset resp.http.X-Backend;
    }
    if (obj.hits > 0) {
        set resp.http.X-Cache = resp.http.X-Cache + ":HIT:" + obj.hits;
    }
    else {
        set resp.http.X-Cache = resp.http.X-Cache + ":MISS";
    }
    if (req.http.X-grace) {
        set resp.http.X-Cache = resp.http.X-Cache + ":Grace:" + req.http.X-grace;
    }
    if (req.http.X-TTL) {
        set resp.http.X-Cache = resp.http.X-Cache + ":TTL:" + req.http.X-TTL;
    }
    if (req.http.x-user-context-hash) {
        set resp.http.X-Cache = resp.http.X-Cache + ":User-Hash:" + req.http.x-user-context-hash;
    }
    // Remove tag headers when delivering to non debug client
    unset resp.http.xkey;
    // Sanity check to prevent ever exposing the hash to a non debug client.
    unset resp.http.x-user-context-hash;
    {{ if .Values.settings.noindex }}
    set resp.http.X-Robots-Tag = "noindex";
    {{ end }}
}

sub vcl_pipe {
    if (req.http.upgrade) {
        set bereq.http.upgrade = req.http.upgrade;
    }
    set bereq.http.connection = "close";
    return (pipe);
}

sub vcl_backend_error {
    std.syslog( 2, "vcl_backend_error on Host " + bereq.http.host + " with URL " + bereq.url );

    return (deliver);
}
sub vcl_synth {
    // Varnish redirect 
    if (resp.status == 750) {
        set resp.status = 301;
        set resp.http.Location = "http://" + req.http.host + req.url;
        return(deliver);
    }
    // Maintainance page 
    if (resp.status == 751) {
        set resp.status = 503;
        set resp.http.X-Robots-Tag = "noindex";
        set resp.http.X-Client-IP = client.ip;
        set resp.http.Content-Type = "text/html; charset=utf-8";
        synthetic(std.fileread("/etc/varnish/maintainance.html"));
        return(deliver);
    }
    {{ if .Values.settings.bot_detect }}
    if (resp.status == 751) {
        set resp.status = 429;
        set resp.http.Content-Type = "text/html; charset=utf-8";
        set resp.http.Retry-After = "3600";
    }
    {{ end }}
    set resp.http.Content-Type = "text/html; charset=utf-8";
    set resp.http.Retry-After = "5";
    set resp.http.Cache-Control = "no-cache,no-store";
    set resp.http.X-Robots-Tag = "none";
    synthetic( {"{{.Values.settings.error}}"} );
    return (deliver);
}
