// Handle purge
// You may add FOSHttpCacheBundle tagging rules
// See http://foshttpcache.readthedocs.org/en/latest/varnish-configuration.html#id4
sub ez_purge {
    // Retrieve purge token, needs to be here due to restart, match for PURGE method done within
    call ez_invalidate_token;

    # Adapted with acl from vendor/friendsofsymfony/http-cache/resources/config/varnish/fos_tags_xkey.vcl
    if (req.method == "PURGEKEYS") {
        call ez_purge_acl;

        # If neither of the headers are provided we return 400 to simplify detecting wrong configuration
        if (!req.http.xkey-purge && !req.http.xkey-softpurge) {
            return (synth(400, "Neither header XKey-Purge or XKey-SoftPurge set"));
        }

        # Based on provided header invalidate (purge) and/or expire (softpurge) the tagged content
        set req.http.n-gone = 0;
        set req.http.n-softgone = 0;
        if (req.http.xkey-purge) {
            set req.http.n-gone = xkey.purge(req.http.xkey-purge);
        }

        if (req.http.xkey-softpurge) {
            set req.http.n-softgone = xkey.softpurge(req.http.xkey-softpurge);
        }

        return (synth(200, "Purged "+req.http.n-gone+" objects, expired "+req.http.n-softgone+" objects"));
    }

    # Adapted with acl from vendor/friendsofsymfony/http-cache/resources/config/varnish/fos_purge.vcl
    if (req.method == "PURGE") {
        call ez_purge_acl;

        return (purge);
    }
}

sub ez_purge_acl {
    if (req.http.x-invalidate-token) {
        if (req.http.x-invalidate-token != req.http.x-backend-invalidate-token) {
            return (synth(405, "Method not allowed"));
        }
    } else if  (!client.ip ~ invalidators) {
        return (synth(405, "Method not allowed"));
    }
}
{{ if .Values.settings.ezplatform.hash }}
// Sub-routine to get client user context hash, used to for being able to vary page cache on user rights.
sub ez_user_context_hash {

    // Prevent tampering attacks on the hash mechanism
    if (req.restarts == 0
        && (req.http.accept ~ "application/vnd.fos.user-context-hash"
            || req.http.x-user-context-hash
        )
    ) {
        return (synth(400));
    }

    if (req.restarts == 0 && (req.method == "GET" || req.method == "HEAD")) {
        // Backup accept header, if set
        if (req.http.accept) {
            set req.http.x-fos-original-accept = req.http.accept;
        }
        set req.http.accept = "application/vnd.fos.user-context-hash";

        // Backup original URL
        set req.http.x-fos-original-url = req.url;
        set req.url = "/_fos_user_context_hash";

        // Force the lookup, the backend must tell not to cache or vary on all
        // headers that are used to build the hash.
        return (hash);
    }

    // Rebuild the original request which now has the hash.
    if (req.restarts > 0
        && req.http.accept == "application/vnd.fos.user-context-hash"
    ) {
        set req.url = req.http.x-fos-original-url;
        unset req.http.x-fos-original-url;
        if (req.http.x-fos-original-accept) {
            set req.http.accept = req.http.x-fos-original-accept;
            unset req.http.x-fos-original-accept;
        } else {
            // If accept header was not set in original request, remove the header here.
            unset req.http.accept;
        }

        // Force the lookup, the backend must tell not to cache or vary on the
        // user context hash to properly separate cached data.

        return (hash);
    }
}
{{ end }}
// Sub-routine to get invalidate token.
sub ez_invalidate_token {
    // Prevent tampering attacks on the token mechanisms
    if (req.restarts == 0
        && (req.http.accept ~ "application/vnd.ezplatform.invalidate-token"
            || req.http.x-backend-invalidate-token
        )
    ) {
        return (synth(400));
    }

    if (req.restarts == 0 && (req.method == "PURGE" || req.method == "PURGEKEYS") && req.http.x-invalidate-token) {
        set req.http.accept = "application/vnd.ezplatform.invalidate-token";

        // Backup original http properties
        set req.http.x-fos-token-url = req.url;
        set req.http.x-fos-token-method = req.method;

        set req.url = "/_ez_http_invalidatetoken";

        // Force the lookup
        return (hash);
    }

    // Rebuild the original request which now has the invalidate token.
    if (req.restarts > 0
        && req.http.accept == "application/vnd.ezplatform.invalidate-token"
    ) {
        set req.url = req.http.x-fos-token-url;
        set req.method = req.http.x-fos-token-method;
        unset req.http.x-fos-token-url;
        unset req.http.x-fos-token-method;
        unset req.http.accept;
    }
}