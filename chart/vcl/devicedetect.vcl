# varnish bot detection

sub vcl_miss {   
    if(req.http.uagent == "bot")
    {
      set req.http.X-uagent = true;
      return (synth(751, "BOT."));
    }
}

backend bot {
    .host = "127.0.0.1";
    .port = "60078";
    .connect_timeout = 3s;
    .first_byte_timeout = 120s;
    .between_bytes_timeout = 120s;
    .probe = {
        .timeout = 1s;
        .interval = 5y;
        .window = 10;
        .threshold = 8;
    }
}

sub devicedetect {
	unset req.http.X-UA-Device;
	set req.http.X-UA-Device = "pc";
	# Handle that a cookie may override the detection alltogether.
	if (req.http.Cookie ~ "(?i)X-UA-Device-force") {
		/* ;?? means zero or one ;, non-greedy to match the first. */
		set req.http.X-UA-Device = regsub(req.http.Cookie, "(?i).*X-UA-Device-force=([^;]+);??.*", "\1");
		/* Clean up our mess in the cookie header */
		set req.http.Cookie = regsuball(req.http.Cookie, "(^|; ) *X-UA-Device-force=[^;]+;? *", "\1");
		/* If the cookie header is now empty, or just whitespace, unset it. */
		if (req.http.Cookie ~ "^ *$") { unset req.http.Cookie; }
	} else {
		if (req.http.User-Agent ~ "(?i)(ads|google|bing|msn|yandex|baidu|ro|career|seznam|)bot" ||
		    req.http.User-Agent ~ "(?i)(baidu|jike|symantec)spider" ||
		    req.http.User-Agent ~ "(?i)scanner" ||
		    req.http.User-Agent ~ "(?i)(web)crawler" ||
			req.http.User-Agent ~ "\(compatible; Googlebot-Mobile/2.1; \+http://www.google.com/bot.html\)" ||
            req.http.User-Agent ~ "(Android|iPhone)" && req.http.User-Agent ~ "\(compatible.?; Googlebot/2.1.?; \+http://www.google.com/bot.html" ||
			req.http.User-Agent ~ "(iPhone|Windows Phone)" && req.http.User-Agent ~ "\(compatible; bingbot/2.0; \+http://www.bing.com/bingbot.htm") {
			set req.http.X-UA-Device = "bot"; }
	}
	if (req.http.X-UA-Device ~ "^bot") {
		set req.http.time = now;
		if(req.http.time ~ "^.*(0[6-9]|1[0-9]|2[0-2]):[0-9][0-9]:[0-9][0-9].* GMT")
		{
			set req.backend_hint = bot;
			set req.http.uagent = "bot";
		}
		else {
			set req.backend_hint = default;
		}
		unset req.http.time;
	}
}

