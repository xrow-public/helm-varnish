#Block donottrack urls
if (req.url ~ "\?.*donottrack.*") {
    return (synth (410, "Invalid URL blocked"));
}

# remove cache breaker
if (req.url ~ "(\?_=.*)") {
    set req.url = regsub(req.url, "(\?_=([A-z0-9_\-\.%25]+))", "");
}

# remove facebook
if (req.url ~ "(\?fbclid=.*)") {
    set req.url = regsub(req.url, "(\?fbclid=([^?]+))", "");
}

# Remove Google Analytics parameters
if (req.url ~ "(\?|&)gclid=") {
    set req.url = regsuball(req.url, "&gclid=([A-z0-9_\-\.%25]+)", "");
    set req.url = regsuball(req.url, "\?gclid=([A-z0-9_\-\.%25]+)", "?");
    set req.url = regsub(req.url, "\?&", "?");
    set req.url = regsub(req.url, "\?$", "");
}